This is a Composer-based installer for the EndlessThemes Drupal distribution.

## Get Started
```
$ composer create-project endlessthemes/endless-project MY_PROJECT
$ cd MY_PROJECT
$ composer install
```
